var cart = {

    data : {},
    rates : {},
    commissions : {},
    commissions_fix : {},
    commissions_fix : {},
    min_USDs : {},
    min_DYC : 1,
    min_USD : 10,
    timeout : null,
    timeoutSave : null,
    intervalum : 500,
    intervalumSave : 5000,
    DYC :0,
    USD : 0,
    lang_now  : 'en',
    payment : '',
    wallet : '',
    step : 1,
    step2Cls : '.office-form-control-box.coin-title',
    step2ClsClass : 'office-form-control-box--selected',

    Init : function(rates_json,step){
        var jsn=JSON.parse(rates_json);
        cart.rates=jsn.rates;
        cart.commissions=jsn.comissions;
        cart.commissions_fix=jsn.comissions_fix;
        cart.min_USDs=jsn.min_USDs;
        cart.lang_now=$('html').attr('lang');
        cart.step=step;
    },

    Send : function(obj){
        var params=$(obj).serialize();
        $.getJSON(CartUrl, params, function( data ) {
            if (data.html!="")  $('.account-container-ajax').html(data.html);
            cart.step=data.step;
            if (data.step==1) cart.InitStep1();
            if (data.step==2) cart.InitStep2();
            if (data.step==3) cart.InitStep3();
            if (data.act=='goto' && data.url!='')  document.location=data.url;
            if (data.act=='popup' && data.url!='') window.open(data.url,"_blank");
        });
        return false;
    },

    Url : function(sbm){
        var params={sbm:sbm, lang: cart.lang_now, DYC: cart.DYC};

        $.getJSON(CartUrl, params, function( data ) {
                if (data.html!="" ) $('.account-container-ajax').html(data.html);
                cart.step=data.step;
                if (data.step==1) cart.InitStep1();
                if (data.step==2) cart.InitStep2();
                if (data.step==3) cart.InitStep3();
                if (data.act=='goto' && data.url!='')  document.location=data.url;
                if (data.act=='popup' && data.url!='') window.open(data.url,"_blank");
        });
        return false;
    },

    InitStep1 : function(){
        console.log("init step 1");
        clearTimeout(cart.timeoutSave);
        clearTimeout(cart.timeout);
        $("#OfficeCartStep1").find('button').attr("disabled","disabled");
        $("#OfficeCartStep1").on("submit",function(ev){
            cart.Send("#"+ev.target.id);
            return false;
        });
        $("#OfficeCartStep1 input").on("keyup",function(ev) {
            //if (ev.keyCode==38) cart.IncreaseValue("#"+ev.target.id);
            //if (ev.keyCode==40) cart.DecreaseValue("#"+ev.target.id);
            clearTimeout(cart.timeout);
            cart.timeout=setTimeout(function(){ cart.CountStep1(ev.target.id); }, cart.intervalum);
        });
        cart.timeout=setTimeout(function(){ cart.CountStep1('DYC'); }, cart.intervalum);

        console.log("init step 1_");
    },

    InitStep2 : function() {
        console.log("init step 2");
        clearTimeout(cart.timeoutSave);
        clearTimeout(cart.timeout);
        $("#OfficeCartStep2").find('button').attr("disabled", "disabled");

        $("#OfficeCartStep2 input").on("keyup", function (ev) {
            clearTimeout(cart.timeout);
            cart.timeout = setTimeout(function () {
                cart.CountStep2(ev.target.id);
            }, cart.intervalum);
        });

        $(cart.step2Cls).filter(":not(.disabled)").on('click', function () {
            cart.selectPay(this);
            cart.CountStep2('DYC');
        });

        cart.timeout = setTimeout(function () {
            cart.selectPay();
            cart.CountStep2('DYC');
        }, 100);

        if ($('.calculator-popup').length > 0) {
            main.calculatorPopup();
        }
        else {
            $("#OfficeCartStep2").on("submit", function (ev) {
                cart.Send("#" + ev.target.id);
                return false;
            });
        }


        if ($("html").hasClass('is-user-logged-out')) {
            $('.login-dialog').on('click', main.loginDialog);
        }


        console.log("init step 2_");
    },

    InitStep3 : function(){
        console.log("init step 3");
        clearTimeout(cart.timeoutSave);
        clearTimeout(cart.timeout);
        var id=$('.DYC-coin').first().attr('id');
        main.copyCode({
            buttonEl: id+'-btn',
            sourceEl: id+'',
            tooltipEl:id+'-tooltip'
        });
        main.copyCode({
            buttonEl: 'qr-code-copy-btn',
            sourceEl: 'qr-code-copy-source',
            tooltipEl:'qr-code-copy-tooltip'
        });

        $("#OfficeCartStep3").find('button').attr("disabled","disabled");
        $("#OfficeCartStep3").on("submit",function(ev){
            cart.Send("#"+ev.target.id);
            return false;
        });

        $("#OfficeCartStep3 input").on("keyup",function(ev) {
            clearTimeout(cart.timeout);
            cart.timeout=setTimeout(function(){ cart.CountStep2(ev.target.id); }, cart.intervalum);


            clearTimeout(cart.timeoutSave);
            if (cart.setSave()) cart.timeoutSave=setTimeout(function(){ cart.Send("#OfficeCartStep3"); }, cart.intervalumSave);
        });

        cart.timeout=setTimeout(function(){ cart.CountStep2('DYC'); cart.selectPay(); }, 100);
        if ($("input[name='payment']").val()=="PAYPAL") cart.InitStep3PayPal();
        if ($("input[name='payment']").val()=="BANK") cart.InitStep3Bank();
        if ($("input[name='payment']").val()=="COINGATE") cart.InitStep3CoinGate();

    },

    setSave: function(){
        if ($("input[name='payment']").val()=="COINGATE") return false;
        if ($("input[name='payment']").val()=="CARD") return false;
        return(true);
    },

    InitStep3PayPal : function(){


        paypal.Button.render({
            env: 'production', // Or 'sandbox', production

            client: {
                sandbox:    'AbGJPYs7HpAe4biLIQRwWlo8S5FG8u2F7myNtU4Cg_fstcd-Xwqk4jj_rmf4TjC3hwnVsQW62jhPZS4c',
                production: 'AYpH7xvpZROcnkxLP2KRyBqVr-RHIHvMTUB1x_rYLA3o1ovi4ZXcCrqF-47ioVCTBwZaVKSJpG74kFgi'
            },

            commit: true, // Show a 'Pay Now' button

            locale: $('html').data('lang-spec'),

            style: {
                color: 'blue',
                size: 'large'
            },

            payment: function(data, actions) {
                // Make a call to the REST api to create the payment

                var coin = parseFloat(cart.USD);
                var _commission=0;
                if ('PAYPAL' in cart.commissions) _commission=_commission+((coin*cart.commissions['PAYPAL']/100));
                if ('PAYPAL' in cart.commissions_fix) _commission=_commission+cart.commissions_fix['PAYPAL'];
                coin=coin + _commission;
                console.log (coin.toFixed(2));

                return actions.payment.create({
                    payment: {
                        transactions: [
                            {
                                amount: { total: coin.toFixed(2), currency: 'USD' }
                            }
                        ]
                    }
                });
            },

            onAuthorize: function(data, actions) {
                console.log(data);
                return actions.payment.execute().then(function(payment) {
                    console.log(payment);
                    if (payment.state == "approved")
                    {
                        $('#wallet').val(payment.id);
                        cart.Send('#OfficeCartStep3');
                    }
                });


            },

            onCancel: function(data, actions) {

            },

            onError: function(err) {
                /*
                 * An error occurred during the transaction
                 */
            }
        }, '#paypal-button');
    },

    InitStep3Bank : function(){
        if ($('#bank-download-btn').length>0)
        {
            setTimeout(function(){
               window.open($('#bank-download-btn').attr('href'),"_blank");
            },1000);

        }
    },

    InitStep3CoinGate : function(){

    },


    IncreaseValue : function (obj){
        var old_value=parseFloat($(obj).val().replace("$",""));
        if (!old_value) old_value=0;
        old_value++;
        $(obj).val(old_value);
    },

    DecreaseValue : function (obj){
        var old_value=parseFloat($(obj).val().replace("$",""));
        if (!old_value) old_value=0;
        old_value++;
        $(obj).val(old_value);
    },

    CountStep1 : function(currensy) {
        console.log("count_step : " + currensy);
        var inp = $("#"+currensy);
        var form = inp.closest("form");

        if (inp.val()=='' || inp.val()==0)
        {

            form.find(".DYC-title").removeClass("office-form-control-box--attention");
            $(".DYC-error-box").hide();
            form.find(".USD-title").removeClass("office-form-control-box--attention");
            $(".USD-error-box").hide();
            $(".DYC-warning-box").show();
            $(".USD-warning-box").show();
            form.find("#DYC").val('');
            form.find("#USD").val('');
            form.find('button').attr("disabled","disabled");
            return;
        }
        else
        {
            $(".DYC-warning-box").hide();
            $(".USD-warning-box").hide();
        }
        var c_count = inp.val();
        var DYC_count = cart.min_DYC;
        var USD_count = cart.min_USD;
        if (currensy == "DYC") {
            DYC_count = parseInt(inp.val());
        }
        else
        {
            DYC_count = parseFloat(inp.val().replace("$","")) / cart.rates['USD'];

        }
        if (!DYC_count) { DYC_count=0; }
        DYC_count = Math.ceil(DYC_count);
        USD_count = (DYC_count * cart.rates['USD']).toFixed(2);

        cart.DYC=DYC_count;
        cart.USD=USD_count;

        form.find("#DYC").val(DYC_count);
        form.find('button').removeAttr('disabled');
        if (DYC_count < cart.min_DYC)
        {
            form.find(".DYC-title").addClass("office-form-control-box--attention");
            $(".DYC-error-box").show();

            form.find('button').attr("disabled","disabled");
        }
        else
        {
            form.find(".DYC-title").removeClass("office-form-control-box--attention");
            $(".DYC-error-box").hide();
        }

        form.find("#USD").val("$"+USD_count);
        if (USD_count < cart.min_USD)
        {
            form.find(".USD-title").addClass("office-form-control-box--attention");
            $(".USD-error-box").show();

            form.find('button').attr("disabled","disabled");
        }
        else
        {
            form.find(".USD-title").removeClass("office-form-control-box--attention");
            $(".USD-error-box").hide();
        }
        cart.changeUrl();
    },

    CountStep2 : function(currensy) {
        console.log("count_step 2 : " + currensy);
        var inp = $("#" + currensy);
        var cur = inp.data('currency');

        var form = inp.closest("form");

        if (inp.val() == '') {
            form.find(".coin-title").removeClass("office-form-control-box--attention");
            $(".DYC-warning-box").show();
            form.find("#DYC").val('');
            form.find(".DYC-coin").val('');
            form.find('button').attr("disabled", "disabled");
            return;
        }
        else {
            $(".DYC-warning-box").hide();
        }

        var c_count = inp.val();
        var DYC_count = cart.min_DYC;
        var USD_count = cart.min_USD;

        if (currensy == "DYC") {
            DYC_count = parseInt(inp.val());
        }
        else {
            var coin = parseFloat(inp.val().replace("$", ""));
            var commission=0;
            var  c=0;
            var cf=0;
            if (currensy in cart.commissions) c = coin * (cart.commissions[currensy]/100);
            if (currensy in cart.commissions_fix) cf =cart.commissions_fix[currensy];
            var ccoin = (100 * coin)/(coin + c + cf)/100;
            DYC_count = parseFloat(ccoin * coin) / cart.rates[cur];

        }
        if (!DYC_count) {
            DYC_count = 0;
        }
        DYC_count = Math.ceil(DYC_count);
        USD_count = (DYC_count * cart.rates['USD']).toFixed(2);

        cart.DYC = DYC_count;
        cart.USD = USD_count;

        form.find("#DYC").val(DYC_count);
        form.find('button').removeAttr('disabled');

        if (DYC_count < cart.min_DYC || USD_count < cart.min_USD) {
            form.find(".DYC-title").addClass("office-form-control-box--attention").removeClass('office-form-control-box--success');
            $(".DYC-error-box").show();
            $(".USD-error-box").show();
            form.find('button').attr("disabled", "disabled");
        }
        else {
            form.find(".DYC-title").removeClass("office-form-control-box--attention").addClass('office-form-control-box--success');
            $(".DYC-error-box").hide();
            $(".USD-error-box").hide();
        }

        form.find('.DYC-coin').each(function () {
            var _cur =  $(this).data('currency');
            var _pm =   $(this).attr('id');
            if (!cart.rates[_cur]) {
                $(this).closest('div.office-card__form-row').hide();
            }
            else {
                var coin = cart.rates[_cur] * cart.DYC;
                var _commission=0;
                if (_pm in cart.commissions) _commission=_commission+((coin*cart.commissions[_pm]/100));
                if (_pm in cart.commissions_fix) _commission=_commission+cart.commissions_fix[_pm];

                coin= coin + _commission;

                $(this).closest('div.office-card__form-row').show();

                if (_cur == "USD") {
                    var coin_fix = '$' + coin.toFixed(2);
                }
                else {
                    var coin_fix = coin.toFixed(6);
                }


                $(this).val(coin_fix);
                form.find('.'+ _cur+'-coin-value-text').html(coin_fix);
            }

        });

        form.find('.DYC-coin-value-text').html(cart.DYC);

        cart.changeUrl();

    },

    selectPay : function(obj) {

        var cont = $(obj);
        if (cont.length==0)
        {
            cont=$(cart.step2Cls+'.'+cart.step2ClsClass);
        }


        $(cart.step2Cls).removeClass(cart.step2ClsClass);
        cont.addClass(cart.step2ClsClass);

        var form = cont.closest("form");
        var payment=cont.find('input.DYC-coin').attr('id');

        if (!payment || payment=='')
        {
            $(cart.step2Cls).first().click();
        }
        else
        {
            console.log('payment: '+payment);
            cart.payment=payment;
            form.find("input[name='payment']").val(cart.payment);
        }

        if (cart.payment in cart.min_USDs) cart.min_USD=cart.min_USDs[cart.payment];
        $(".cart-min-usd-number").html("$"+cart.min_USD);


    },
    changeUrl : function(){
        var parsedUrl = $.url(window.location.href);
        var params = parsedUrl.param();
        params["DYC"] = cart.DYC;
        params["sbm"] = cart.step-1;
        if (cart.payment!='') params['payment']=cart.payment;
        var newUrl = "?" + $.param(params);

        if (typeof (history.pushState) != "undefined") {
            var obj = { Page: $(document).find("title").text(), Url: newUrl };
            history.pushState(obj, obj.Page, obj.Url);
        }
    }


};