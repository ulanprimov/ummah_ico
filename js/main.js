var Main = function () {};
var httpUrl = 'https://ico.dylyver.com';

Main.prototype.init = function () {
    this.smartHeader('header', 100);
    this.userSettings();
    this.subNav();
    this.subNavMainMenu();
    this.dropdownRightOffsetByBtn();
    this.wow();
    this.headerTop();
    this.lang();
    this.nav();
    this.scrollTo();
    this.btnClickFocusout();
    this.faq();
    this.slick();
    this.countdownClose();
    this.scrollNavAllClose();
    this.tabs();
    this.officeBountyDisplayAndScroll();
    this.officeBountyChangeContent();
    this.officeTransactionType();
    this.OfficeBountyForm();
    this.OfficeBountyDelete();
};

Main.prototype.initLoad = function () {
    this.countdownDisplay();
};

Main.prototype.userSettings = function () {

    var $html, $context, $el, cls;

    $context = $('.user-settings');

    if ($context.length === 0) return;

    $html = $('html');
    $el = $context.find('.user-settings__img-link');
    cls = 'is-dropdown-nav';

    $el.on('click', function (e) {
        if ($html.hasClass(cls)) {
            $html.removeClass(cls);
        } else {
            $html.addClass(cls);
        }

        this.blur();
        e.preventDefault();
    });

    $(document).on('click', function (e) {
        if ($(e.target).closest('.main-nav--dropdown').length > 0 ||
            $(e.target).closest('.user-settings').length > 0) return;
        $html.removeClass(cls);
    });

    $(window).on('scroll', function () {
        $html.removeClass(cls);
    });
};

Main.prototype.debounce = function(func, wait, immediate) {
    var timeout;
    return function() {
        var context, args, later, callNow;
        context = this;
        args = arguments;
        later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

Main.prototype.throttle = function (fn, wait) {
    var time = Date.now();
    return function() {
        if ((time + wait - Date.now()) < 0) {
            fn();
            time = Date.now();
        }
    }
};

Main.prototype.triggerEvent = function(obj, eventName) {
    var event = document.createEvent('HTMLEvents');
    event.initEvent(eventName, true, false);
    obj.dispatchEvent(event);
};

Main.prototype.subNav = function () {

    var $itemHasSubNav = $('.main-nav__item--has-sub');
    var $triggerSubNav = $itemHasSubNav.children('.main-nav__link--trigger-sub');
    var $itemSub = $itemHasSubNav.find('.main-nav__item--sub');

    $triggerSubNav.on('click', function (e) {

        $(this)
            .next()
            .toggleClass('main-nav__list--sub-expanded');
    });

    $itemHasSubNav.on('click', '.main-nav__list--sub-expanded', function () {
        $(this).removeClass('main-nav__list--sub-expanded');
    });

    $itemSub.on('click', function () {
        $('html').removeClass('is-dropdown-nav');
    });
};

Main.prototype.subNavMainMenu = function () {

    var $header, $dropdown, $item;

    $header = $('.header');
    $dropdown = $header.find('.main-nav--dropdown.main-nav--sub');
    $item = $dropdown.closest('.main-nav__item');

    $item.on('click', function () {

        var $el, cls;

        $el = $(this);
        cls = 'main-nav__item--sub-active';

        if ($el.hasClass(cls)) {
            $el.removeClass(cls);
        } else {
            $el.addClass(cls);
        }
    });

    $(document).on('click', function (e) {
        if ($(e.target).closest('.main-nav__item--sub-active').length > 0) return;
        $header.find('.main-nav__item--sub-active').removeClass('main-nav__item--sub-active');
    });
};

Main.prototype.dropdownRightOffsetByBtn = function () {

    var that, $header, $dropdown;

    that = this;
    $header = $('.header');
    $dropdown = $header.find('.main-nav + .main-nav--dropdown');

    function handler() {
        if (Modernizr.mq('(min-width: 668px)')) {
            var $token, $lang, langWidth, tokenWidth, elsWidth;
            $token = $header.find('.token');
            if ($token.length === 0) return;
            $lang = $header.find('.lang');

            langWidth = $token.width();
            tokenWidth = $lang.width();
            elsWidth = langWidth + tokenWidth;
            $dropdown.css('right',  elsWidth);
        } else {
            $dropdown.css('right',  '');
        }
    }

    $(window).on('resize', that.throttle(handler, 200));
    handler();
};

Main.prototype.smartHeader = function (clsHeader, offsetToMiniHeader) {

    if (!document.querySelector('.' + clsHeader)) return;

    var that, header, headerHeight, clsHeaderTop;

    that = this;
    clsHeader = clsHeader || 'header';
    offsetToMiniHeader = offsetToMiniHeader || 100;
    header = document.querySelector('.' + clsHeader);
    headerHeight = header.offsetHeight;
    clsHeaderTop = clsHeader + '--visible';

    header.classList.add(clsHeader + '--smart');

    function getWindowScrollY() {
        return window.scrollY || window.pageYOffset || document.documentElement.scrollTop;
    }

    function setMiniClassByOffset() {
        if (getWindowScrollY() > offsetToMiniHeader && header.classList.contains(clsHeader + '--visible')) {
            header.classList.add(clsHeader + '--mini');
        } else {
            header.classList.remove(clsHeader + '--mini');
        }
    }

    function setHeaderDisplay(args) {

        var position, scroll;

        position = getWindowScrollY();

        function scrollHandler() {
            scroll = getWindowScrollY();
            if (scroll > position) {
                args.down();
            } else {
                args.up();
            }
            position = scroll;
        }
        window.addEventListener('scroll', that.throttle(scrollHandler, 20));
        scrollHandler();
    }

    function showHeader() {
        header.classList.add(clsHeaderTop);
    }

    function hideHeader() {
        header.classList.remove(clsHeaderTop);
    }

    setHeaderDisplay({
        down: function() {
            if (getWindowScrollY() > headerHeight) {
                hideHeader();
            }
        },
        up: function() {
            showHeader();
        }
    });

    window.addEventListener('scroll', that.throttle(setMiniClassByOffset, 100));
    window.addEventListener('scroll', that.debounce(setMiniClassByOffset, 100));
    that.triggerEvent(window, 'scroll');
};


Main.prototype.wow = function() {
    if ('ontouchstart' in window || navigator.maxTouchPoints) return;
    new WOW().init();
};

Main.prototype.headerTop = function () {

    var $el, $btn, clsHidden;

    $el = $('.header-top');

    if ($el.length === 0) return;

    $btn = $el.find('.header-top__close-btn');
    clsHidden = 'is-hidden';

    if (!sessionStorage.getItem('header-top')) $el.removeClass('header-top--is-hidden');

    $btn.on('click', function () {
        $el.slideUp(200);
        if (sessionStorage.getItem('header-top')) return;
        sessionStorage.setItem('header-top', '1');
    });
};

Main.prototype.lang = function () {

    var $context, $btn, $list, $items, $links, clsActive, clsHidden;

    $context = $('.lang');

    if ($context.length === 0) return;
    $btn = $context.find('.lang__btn');
    $list = $context.find('.lang__list');
    $items = $list.find('.lang__item');
    $links = $items.find('.lang__link');
    clsActive = 'is-active';
    clsHidden = 'is-hidden';

    $btn.on('click', function (e) {
        e.preventDefault();
        ($context.hasClass(clsActive)) ?
            $context.removeClass(clsActive) :
            $context.addClass(clsActive);
    });

    $links.on('click', function () {

        var $el, lang;

        $el = $(this);
        lang = $el.data('lang');

        $items.removeClass(clsHidden);
        $el.closest('.lang__item').addClass(clsHidden);
        $btn.text(lang);
        $context.removeClass(clsActive);
    });

    $(document).on('click', function (e) {
        if ($(e.target).closest('.lang').length > 0) return;
        $context.removeClass(clsActive);
    });

    $(window).on('scroll', function () {
        $context.removeClass(clsActive);
    });
};

Main.prototype.getScrollbarMeasure = function () {
    var scrollDiv, scrollbarWidth;
    scrollDiv = document.createElement('div');
    scrollDiv.className = 'scrollbar-measure';
    document.body.appendChild(scrollDiv);
    scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    document.body.removeChild(scrollDiv);
    return scrollbarWidth;
};

Main.prototype.nav = function () {

    var $body, $el, $btnOpen, $btnClose, scrollerWidth, transitionEvent, cls;

    $body = $('html, body');
    $el = $('.nav-all');
    $links = $el.find('.nav-all__boxes a');
    $btnOpen = $('.navbar-toggle__link');
    $btnClose = $('.nav-close__link');
    scrollerWidth = (this.isMobile) ? 0 : this.getScrollbarMeasure();
    scrollHeight = '100%';
    transitionEvent = 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend';
    cls = 'is-nav-all--expanded';

    function openHandler(e) {
        $body.addClass(cls);
        $el.css({ 'padding-right': scrollerWidth, 'overflow-y': '' });
        $el.on(transitionEvent , function() {
            $body.css({ 'overflow-y': 'hidden' });
            $el.css({'overflow-y': 'scroll'});
        });
        e.preventDefault();
    }

    function closeHandler(e) {
        $body.removeClass(cls).css({ 'overflow-y': '' });
        $el.css({ 'padding-right': '', 'overflow-y': 'hidden' });
        $el.on(transitionEvent , function() {
            $body.css({ 'overflow-y': '' });
        });
        if (e) e.preventDefault();
    }

    $btnOpen.on('click', openHandler);
    $btnClose.on('click', closeHandler);

    $links.on('click', function () {
        $btnClose.triggerHandler('click');
    });

    $(document).on('keydown', function (e) {
        if (e.which !== 27) return;
        closeHandler();
    });
};

Main.prototype.scrollTo = function () {

    function removeHash() {
        var scrollV, scrollH, loc = window.location;
        if ("pushState" in history)
            history.pushState("", document.title, loc.pathname + loc.search);
        else {
            scrollV = document.body.scrollTop;
            scrollH = document.body.scrollLeft;
            loc.hash = '';
            document.body.scrollTop = scrollV;
            document.body.scrollLeft = scrollH;
        }
    }

    var $btn, clsName, $el, body, hash;

    body = $('html, body');
    $btn = $('.scrolling-btn');

    function scrollTo(clsName) {
        $el = $('.' + clsName);

        if (!$el.offset()) return;

        body
            .stop()
            .animate({
                scrollTop: $el.offset().top
            }, 500, 'swing');
    }

    $btn.on('click', function (e) {

        clsName = $(this).data('scroll-to');
        scrollTo(clsName);
        e.preventDefault();
        return false;
    });

    function hashHandler() {

        if ($('.office-content').length > 0) return;

        hash = window.location.hash;
        hash = hash.replace(/^#/, '');

        if (hash) {
            clsName = hash;
            scrollTo(clsName);
            removeHash();
        }
    }

    hashHandler();

    window.addEventListener('hashchange', function () {
        hashHandler();
    });

};

Main.prototype.btnClickFocusout = function () {
    var $els = $('.btn');
    if ($els.length === 0) return;
    $els.on('click', function () {
        this.blur();
    });
};

Main.prototype.faq = function () {

    var $context = $('.faq__content');

    if ($context.length === 0) return;

    var $toggleAll, $asideItemLinks, toggleAllText, $toggleItem, $contentItem, clsActive, categoryName, bool;

    $toggleAll = $('[data-function="toggleAll"]');
    $asideItemLinks = $('.faq__aside-list').find('.faq__aside-link');
    toggleAllText = {
        collapse: $toggleAll.data('label-collapse'),
        expand: $toggleAll.data('label-expand')
    };
    $toggleItem = $('[data-function="toggle"]');
    $contentItem = $('[data-content-item]');
    clsActive = 'is-active';
    bool = true;

    function setToggle() {

        var $btn = $toggleAll.closest($context).filter('.' + clsActive).find($toggleAll);

        bool = true;

        $context.filter('.' + clsActive).find($contentItem).each(function () {
            var $this = $(this);

            if ($this.hasClass(clsActive) === false) {
                bool = false;
                return false;
            }
        });

        if (bool) {
            $btn.text(toggleAllText.collapse);
        } else {
            $btn.text(toggleAllText.expand);
        }
    }

    $toggleAll.on('click', function (e) {

        var $item, $btn;

        $btn = $toggleAll.closest($context).filter('.' + clsActive).find($toggleAll);
        $item = $contentItem.closest($context).filter('.' + clsActive).find($contentItem);

        e.preventDefault();

        setToggle();

        if (bool) {
            $item.removeClass(clsActive);
            $btn.text(toggleAllText.expand);
        } else {
            $item.addClass(clsActive);
            $btn.text(toggleAllText.collapse);
        }

    });

    $toggleItem.on('click', function (e) {

        e.preventDefault();
        $(this).closest('[data-content-item]').toggleClass(clsActive);

        setToggle();
    });
};

Main.prototype.polarToCartesian = function(centerX, centerY, radius, angleInDegrees) {
    var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;

    return {
        x: centerX + (radius * Math.cos(angleInRadians)),
        y: centerY + (radius * Math.sin(angleInRadians))
    };
};

Main.prototype.describeArc = function(x, y, radius, startAngle, endAngle) {

    var start = this.polarToCartesian(x, y, radius, endAngle);
    var end = this.polarToCartesian(x, y, radius, startAngle);
    var largeArcFlag = endAngle - startAngle <= 180 ? '0' : '1';

    var d = [
        'M', start.x, start.y,
        'A', radius, radius, 0, largeArcFlag, 0, end.x, end.y
    ].join(' ');

    return d;
};

Main.prototype.slick = function () {

    var $el = $('[data-slider="true"]');

    if ($el.length === 0) return;

    $el.slick({
        dots: true
    });
};

Main.prototype.tabs = function (cb) {

    var $tabs = $('.tabs');

    if ($tabs.length === 0) return;

    var $navItem, $content, clsActive;

    clsActive = 'is-active';

    $tabs.each(function (i, tab) {
        $navItem = $(tab).find('.tabs__nav-item');
        $content = $(tab).find('.tabs__content');
        $navItem.find('.tabs__nav-link').on('click', function () {
            $navItem.removeClass(clsActive);
            $content.removeClass(clsActive);
            $(this).closest('.tabs__nav-item').addClass(clsActive);
            $content.filter('[data-tab-id="' + (this.href.split('#')[1]) + '"]').addClass(clsActive);
            if (!!cb && typeof cb === 'function') cb();
        });
    });
};

Main.prototype.countdown = function (context, dateEnd) {

    /* dataEnd e.g.: 'Apr 04, 2030 17:22:00' */
    var ctxs = document.querySelectorAll(context);

    if (!ctxs) return;

    var countDownDateTime = new Date("Feb 30, 2019 00:00:0").getTime();

    [].forEach.call(ctxs, function (ctx, i) {

        var timer = setInterval(function () {

            var currentDateTime = new Date().getTime();
            var diff = countDownDateTime - currentDateTime;

            var days = Math.floor(diff / (1000 * 60 * 60 * 24));
            var hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((diff % (1000 * 60)) / 1000);

            if (days < 10) days = '0' + days;
            if (hours < 10) hours = '0' + hours;
            if (minutes < 10) minutes = '0' + minutes;
            if (seconds < 10) seconds = '0' + seconds;

            if (parseInt(days, 10) < 1) days = '00';
            if (parseInt(hours, 10) < 1) hours = '00';
            if (parseInt(minutes, 10) < 1) minutes = '00';
            if (parseInt(seconds, 10) < 1) seconds = '00';

            $(ctx).find('.countdown__days').html(days);
            $(ctx).find('.countdown__hours').html(hours);
            $(ctx).find('.countdown__minutes').html(minutes);
            $(ctx).find('.countdown__seconds').html(seconds);

            $(ctx).find('.countdown__days').next().html(showDay(parseInt(days)));
            $(ctx).find('.countdown__hours').next().html(showHour(parseInt(hours)));
            $(ctx).find('.countdown__minutes').next().html(showMinute(parseInt(minutes)));
            $(ctx).find('.countdown__seconds').next().html(showSecond(parseInt(seconds)));


            if (diff < 1000) {
                clearInterval(timer);
                // countdown completed
            }
        }, 1000);
    });
};

Main.prototype.countdownDisplay = function () {

    var o1, o2, v1, v2, wst, cls;

    o1 = document.querySelector('[data-countdown-begin]');
    o2 = document.querySelector('[data-countdown-end]');

    if (!o1 || !o2) return;

    v1 = o1.offsetTop + o1.offsetHeight * 0.9;
    v2 = o2.offsetTop;
    cls = 'is-countdown-shown';

    function scrollHandler() {
        wst = $(window).scrollTop();

        if (wst > v1 && wst < v2) {
            document.body.classList.add(cls);
        } else {
            document.body.classList.remove(cls);
        }
    }

    scrollHandler();
    $(window).on('scroll', scrollHandler);
};

Main.prototype.countdownClose = function () {

    var ctx = document.querySelector('.countdown--pane');

    if (!ctx) return;

    var btn = ctx.querySelector('.countdown__btn-close');

    if (!btn) return;

    var cls = 'is-hidden';

    btn.addEventListener('click', function () {
        ctx.classList.add(cls);
        document.body.classList.add('is-countdown-shown--close');
    });
};

Main.prototype.newsletterForm = function(mail_form) {
    console.log(mail_form);
    var container=$(mail_form);
    var email_value=container.find('input[type="email"]').val();
    if (email_value=="") return(false);
    $.getJSON( httpUrl + "/ajax/newsletter.php", {email:email_value, lang: $('html').attr('lang'),category_id: 2}, function( data ) {
        if (data.status=="ok") { container.html(data.html); }
        else
        {
            var alert_="";
            for (i in data.error)
            {
                    alert_=alert_ + data.error[i];
            }
            if (alert!='')  alert(alert_);
        }
        return(false);
    });
    return(false);
};

Main.prototype.webinarForm = function(mail_form) {
    console.log(mail_form);
    var container=$(mail_form);
    var email_value=container.find('input[type="email"]').val();
    console.log(email_value);
    if (email_value=="") return(false);
    $.getJSON( httpUrl + "/ajax/webinar.php", {email:email_value, lang: $('html').attr('lang'),category_id: 3}, function( data ) {
        if (data.status=="ok") { container.html(data.html); }
        else
        {
            var alert_="";
            for (i in data.error)
            {
                alert_=alert_ + data.error[i];
            }
            if (alert!='')  alert(alert_);
        }
        return(false);
    });
    return(false);
};

Main.prototype.getScrollbarWidth = function () {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    outer.style.msOverflowStyle = "scrollbar";
    document.body.appendChild(outer);
    var widthNoScroll = outer.offsetWidth;
    outer.style.overflow = "scroll";
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);
    var widthWithScroll = inner.offsetWidth;
    outer.parentNode.removeChild(outer);
    return widthNoScroll - widthWithScroll;
};

Main.prototype.OfficeBountyForm = function(obj){
    $('.office-bounty__form').each(function(){
        $(this).on('submit',function(e){

            var request_data = $(this).serializeFiles();
            var _this=$(this);
            e.preventDefault();


            $('.office-bounty__form_success').hide();
            _this.find('input').each(function(){
                if ($(this).hasClass('office-form-error')) $(this).removeClass('office-form-error')
            });
            _this.find('textarea').each(function(){
                if ($(this).hasClass('office-form-error')) $(this).removeClass('office-form-error')
            });
            _this.find('.office-form-label--error').remove();

            $.ajax({
                url: httpUrl + '/ajax/bounty.php',
                data: request_data,
                cache: false,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                dataType: "json",
                success: function(data){
                    console.log(data);
                    if (data.action=="reload")  {  document.location = document.location.href;  }
                    if (data.action=="goto")    {  document.location = data.url;  }
                    if (data.status=="success") { _this.find("input[type!='hidden']").val(''); _this.hide();  _this.next('.office-bounty__form_success').show();  }
                    if (data.status=="error") {
                        if (_this.find('.office-bounty__form_error').length==0) _this.append('<div class="office-bounty__form_error text-center"></div>');
                        _this.find('.office-bounty__form_error').html('');
                        for (i in data.err)
                        {
                            //_this.find('.office-bounty__form_error').append('<p>'+data.err[i]['description']+'</p>');
                            _this.find('input[name="'+data.err[i]['field']+'"]').addClass('office-form-error');
                            _this.find('input[name="'+data.err[i]['field']+'"]').after('<div class="office-form-label--error">'+data.err[i]['description']+'</div>');

                            _this.find('textarea[name="'+data.err[i]['field']+'"]').addClass('office-form-error');
                            _this.find('textarea[name="'+data.err[i]['field']+'"]').after('<div class="office-form-label--error">'+data.err[i]['description']+'</div>');
                        }

                    }
                    return(false);
                }
            });
            return false;
        });
    });
};

Main.prototype.OfficeBountyDelete = function(){
    $('a.bounty__close-btn').each(function(){
        console.log($(this));
        $(this).on('click',function(e){

            var id = $(this).data('id');
            var question = $(this).data('question');
            var _this=$(this);
            e.preventDefault();

            if (confirm(question))
            {
                $.getJSON(httpUrl + '/ajax/bounty_update.php', {id:id, bbmm:'bbmm'}, function(data){
                        console.log(data);
                        if (data.action=="reload")  {  document.location = document.location.href;  }
                        if (data.action=="goto")    {  document.location = data.url;  }
                        if (data.status=="success") { _this.closest("tr").hide();  }
                        if (data.status=="error") {
                            var error_text='';
                            for (i in data.err)
                            {
                                error_text=error_text + ' ' + data.err[i]['description'];
                                alert(error_text);
                            }

                        }
                        return(false);
                });
            }
            return false;
        });
    });
};

Main.prototype.loginDialog = function(request_data) {
    console.log(request_data);
    if (typeof  request_data != "string") request_data = {lang: $('html').attr('lang')};
    console.log(request_data);
    $.getJSON( httpUrl + "/ajax/user.php", request_data, function( data ) {
        console.log(data);
        $container = $('.modal-dialog');
        $container.filter('[data-modal-dialog-name="modal-dialog"]');
        $container.find('.html-data').html(data.html);
        $container.find('[data-autofocus]').focus();
        if (data.action=="close")  { main.modalDialogClose('modal-dialog'); return; }
        if (data.action=="reload") { main.modalDialogClose('modal-dialog'); document.location = document.location.href; return; }
        if (data.action=="goto") { main.modalDialogClose('modal-dialog'); document.location = data.url; return; }
            main.modalDialogOpen('modal-dialog');
        if (data.action=="form") {
            $container.find(".html-data").find('form').find('input[name="sub2"]').val(1);
            $container.find(".html-data").find('form').submit(function(){
                var request_data=$(this).serialize();
                main.loginDialog(request_data);
                return(false);
            })
        }
        if (data.event=="registration")
        {
            yaCounter48850823.reachGoal('register');
            gtag('event', 'sendemail', { 'event_category': 'registration-forms', 'event_action': 'registration', });
        }

    });
};

Main.prototype.modalDialogOpen = function (name) {

    var $container, $dismiss, $confirm, hideCls, scrollWidth;

    var _this=this;

    $container = $('.modal-dialog');
    $dismiss = $container.find('[data-modal-dialog-role="dismiss"]');
    $confirm = $container.find('[data-modal-dialog-role="confirm"]');
    hideCls = 'is-hidden';
    scrollWidth = _this.getScrollbarWidth();

    $container.filter('[data-modal-dialog-name="' + name + '"]').removeClass(hideCls);
    $('body, .header').css('padding-right', scrollWidth);
    $('html, body').addClass('overflow-hidden');

    $dismiss.off('click.modal-dialog-dismiss').on('click.modal-dialog-dismiss', function () {
        _this.modalDialogClose(name);
        $('body, .header').css('padding-right', '');
        $('html, body').removeClass('overflow-hidden');
    });

    $(document).off('keyup.modal-dialog-dismiss').on('keyup.modal-dialog-dismiss', function (event) {
        if (event.which === 27) {
            $dismiss.triggerHandler('click');
        }
    });

    if ($confirm.length !== 0) {
        $confirm.off('click.modal-dialog-confirm').on('click.modal-dialog-confirm', function () {
            if (typeof cb === 'function') cb(name);
            _this.modalDialogClose(name);
        });
    }
};

Main.prototype.modalDialogClose = function (name) {
    var hideCls = 'is-hidden';
    $container = $('.modal-dialog');
    $container.filter('[data-modal-dialog-name="' + name + '"]').removeClass(hideCls);
    $container.addClass(hideCls);
};

Main.prototype.modalDialog = function (cb) {

    var $container, $dismiss, $trigger, $confirm, name, hideCls;

    $trigger = $('[data-modal-dialog-role="open"]');

    if ($trigger.length === 0) return;

    $container = $('.modal-dialog');
    $dismiss = $container.find('[data-modal-dialog-role="dismiss"]');
    $confirm = $container.find('[data-modal-dialog-role="confirm"]');
    hideCls = 'is-hidden';

    $trigger.off('click.modal-dialog-open').on('click.modal-dialog-open', function () {
        name = $(this).data('modal-dialog-href');
        $container.filter('[data-modal-dialog-name="' + name + '"]').removeClass(hideCls);
    });

    $dismiss.off('click.modal-dialog-dismiss').on('click.modal-dialog-dismiss', function () {
        $container.addClass(hideCls);
    });

    $(document).off('keyup.modal-dialog-dismiss').on('keyup.modal-dialog-dismiss', function (event) {
        if (event.which === 27) {
            $dismiss.triggerHandler('click');
        }
    });

    if ($confirm.length !== 0) {
        $confirm.off('click.modal-dialog-confirm').on('click.modal-dialog-confirm', function () {
            if (typeof cb === 'function') cb(name);
            $container.addClass(hideCls);
        });
    }
};

Main.prototype.scrollNavAllClose = function () {
    var $el, $window, t;
    $el = $('.nav-close');
    $window = $('.nav-all');
    t = 200;

    function setScroll() {
        var mt = 0;
        mt += $window.scrollTop();
        mt = Math.floor(mt);
        $el.stop().animate({'margin-top': mt}, t);
    }

    $window.on('scroll', setScroll);
    $window.on('resize', setScroll);
    setScroll();
};

/*
Main.prototype.tab = function () {

    var $tabs = $('.tab');

    if ($tabs.length === 0) return;

    // Toggle Class: Normal navigation - Hamburger navigation
    function toggleTabsNav (clsParent, clsChildren) {

        var parent, children, cls;

        parent = document.querySelector('.' + clsParent);
        children = document.querySelectorAll('.' + clsChildren);
        cls = 'is-breaked';

        function isBreaked() {

            var len, childrenWidth;

            len = children.length;
            childrenWidth = 0;

            while (len--) {
                childrenWidth += parseInt(children[len].offsetWidth) || 0;
                childrenWidth += parseInt(getComputedStyle(children[len]).getPropertyValue('margin-left')) || 0;
                childrenWidth += parseInt(getComputedStyle(children[len]).getPropertyValue('margin-right')) || 0;
            }

            return (parent.offsetWidth < childrenWidth) ? true : false;
        }


        function toggleClassHandler() {
            parent.classList.remove(cls);
            (isBreaked()) ? parent.classList.add(cls) : parent.classList.remove(cls);
        }

        window.addEventListener('resize', toggleClassHandler);
        toggleClassHandler();
    }

    $tabs.each(function() {
        toggleTabsNav('tab__nav-list', 'tab__nav-item');
    });
};
*/

Main.prototype.copyToClipboard = function (selector) {
    selector.select();
    document.execCommand('copy');
    if (document.selection) {
        document.selection.empty();
    } else if (window.getSelection) {
        window.getSelection().removeAllRanges();
    }
};

// params (object): btnEl, sourceEl, tooltipEl
Main.prototype.copyCode = function (params) {
    var btnEl, srcEl, tpEl, cls, t, that;

    btnEl = document.getElementById(params.buttonEl);
    srcEl = document.getElementById(params.sourceEl);
    tpEl = document.getElementById(params.tooltipEl);
    cls = 'is-hidden';
    that = this;

    if (!btnEl) return;

    btnEl.addEventListener('click', function () {
        that.copyToClipboard(srcEl);
        tpEl.classList.remove(cls);
        t = setTimeout(function () {
            tpEl.classList.add(cls);
        }, 1500);
    });

    tpEl.addEventListener('click', function () {
        clearInterval(t);
        tpEl.classList.add(cls);
    });
};

Main.prototype.calculatorPopup = function () {

    var $popup = $('[data-popup="calculator"]');

    if ($popup.length === 0) return;

    var $showBtn, $closeBtn, clsHidden, scrollWidth;

    $closeBtn = $popup.find('.calculator-popup__btn-close');
    $showBtn = $('[data-popup-open="calculator"]');
    clsHidden = 'is-hidden';
    scrollWidth = this.getScrollbarWidth();

    $closeBtn.on('click', function () {
        $popup.addClass(clsHidden);
        $('body, .header').css('padding-right', '');
        $('html, body').removeClass('overflow-hidden');
    });

    $showBtn.on('click', function () {
        $popup.removeClass(clsHidden);
        $('body, .header').css('padding-right', scrollWidth);
        $('html, body').addClass('overflow-hidden');
    });
};

Main.prototype.officeBountyDisplayAndScroll = function () {

    var _id, clsNav, clsContent, $btn;

    clsNav = 'office-bounty__social-item';
    clsContent = 'office-bounty__social-content-item';

    $btn = $('.' + clsNav);

    if ($btn.length === 0) return;

    $btn.on('click', function () {

        var $this, $content, $contentActive;
        $this = $(this);

        $btn.removeClass(clsNav + '--active');
        $this.addClass(clsNav + '--active');

        _id = $this.data('bounty-social-id');

        $content = $('.' + clsContent);
        $contentActive = $content.filter('[data-bounty-social-id="' + _id + '"]');

        $content.removeClass(clsContent + '--active');
        $contentActive.addClass(clsContent + '--active');

        $('html, body').animate({
            scrollTop: parseInt($contentActive.offset().top, 10) - 30
        }, 1000);
    });
};

Main.prototype.officeBountyChangeContent = function () {

    var $ctx = $('.js-bounty');

    if ($ctx.length === 0) return;

    var $select, $backBtn;

    $select = $ctx.find('.js-bounty-select');
    $backBtn = $('.js-bounty-back');

    $select.each(function() {

        var $this, $context, _id;

        $this = $(this);
        $context = $this.closest('.js-bounty');

        $this.on('change', function () {
            _id = $this.val();
            $context.find('[data-content-id]').addClass('is-hidden');
            $context
                .find('[data-content-id="' + _id + '"]')
                .removeClass('is-hidden')
                .find('.js-bounty-select')
                .val(_id);
        });
    });

    $backBtn.on('click', function() {
        $(this)
            .closest('.js-bounty')
            .find('[data-content-id="-1"]')
            .find('.js-bounty-select')
            .val('-1')
            .triggerHandler('change');
    });
};

Main.prototype.officeTransactionType = function () {

    var $select;
    var $select = $('.js-transaction-select');

    if ($select.length === 0) return;

    $select.each(function() {
        var $this, _id;
        $this = $(this);
        $this.on('change', function () {
            _id = $this.val();
            window.location.href=_id;
        });
    });

};

Main.prototype.preloader = function (display) {
    var preloader, clsHide;
    preloader = document.getElementById('preloader');
    clsHide = 'is-hidden';
    (display) ? preloader.classList.remove(clsHide) : preloader.classList.add(clsHide);
};

Main.prototype.structure = function () {

    var $els = $('.structure__toggle-list-btn');

    if ($els.length === 0) return;

    var $rootList, $rootItem, $firstLevelItems,
        $el, $item,
        ajaxUrl, level, _id,
        clsList, clsItem,
        clsExpand, clsBreaked,
        clsSlided, clsHidden;

    $rootList = $('.structure__list--root');
    $rootItem = $rootList.children('.structure__item--root');
    $firstLevelItems = $rootItem.find('> .structure__list > .structure__item');

    clsList = 'structure__list';
    clsItem = 'structure__item';
    clsExpand = 'is-expanded';
    clsBreaked = 'is-breaked';
    clsSlided = 'is-slided';
    clsHidden = 'is-hidden';

    function isBreaked() {
        return (parseInt($rootList.width()) < 992);
    }

    function breakedHandler() {
        $rootList.removeClass(clsBreaked);
        if (isBreaked())  {
            $rootList.addClass(clsBreaked);
        }
    }

    $rootList.on('click.structure', '.structure__toggle-list-btn', function (e) {

        e.preventDefault();
        e.stopImmediatePropagation();

        $el = $(this);
        $item = $el.closest('.' + clsItem);
        level = $item.data('level');

        var $firstItem = $el.closest($firstLevelItems);

        if ($item.hasClass(clsExpand)) {
            $item.removeClass(clsExpand);
            $item.find('.' + clsItem).removeClass(clsExpand);
            $item.find('.' + clsList).addClass(clsHidden);

            $firstItem.attr('data-expanded-max-level', level);

            if (isBreaked()) {
                $el.closest('.' + clsList).removeClass(clsSlided);
                $el.closest('.' + clsList).prevAll().removeClass(clsHidden);
                $el.closest('.' + clsItem).siblings('.' + clsItem).removeClass(clsHidden);
            }

        } else {
            if ($item.children('.' + clsList).length === 0) {

                ajaxUrl = $item.data('ajax');
                _id = $item.data('id');

                $.ajax({

                    url: ajaxUrl,
                    type: 'get',
                    data: {id: _id},
                    beforeSend: function () {
                        main.preloader(true);
                    }
                }).done(function (response) {
                    if (!response) return;
                    main.preloader(false);
                    $item.append(response);
                });
            } else {
                $item.children('.' + clsList).removeClass(clsHidden);
            }

            $item.addClass(clsExpand);

            $firstItem.attr('data-expanded-max-level', (level + 1));

            if (isBreaked()) {
                $el.closest('.' + clsList).addClass('is-slided');
                $el.closest('.' + clsList).prevAll().addClass(clsHidden);
                $el.closest('.' + clsItem).siblings('.' + clsItem).addClass(clsHidden);
            }

        }
    });

    breakedHandler();
    window.addEventListener('resize', breakedHandler);

};

var main = new Main();

$(document).ready(function () {
    main.init();
});

$(window).on('load', function() {
    main.initLoad();
});


(function($) {
    $.fn.serializeFiles = function() {
        var form = $(this),
            formData = new FormData();
        formParams = form.serializeArray();

        console.log(form);

        $.each(form.find('input[type="file"]'), function(i, tag) {
            $.each($(tag)[0].files, function(i, file) {
                formData.append(tag.name, file);
            });
        });

        $.each(formParams, function(i, val) {
            formData.append(val.name, val.value);
        });

        console.log(formData);

        return formData;
    };
})(jQuery);