function showSecond(number){
    if (number==0) return('segundo');
    if (number==1) return('segundo');
    return('segundos');
}

function showMinute(number){
    if (number==0) return('minutos');
    if (number==1) return('minuto');
    return('minutos');
}

function showHour(number){
    if (number==0) return('hora');
    if (number==1) return('hora');
    return('horas');
}

function showDay(number){
    if (number==0) return('días');
    if (number==1) return('día');
    return('días');
}

function showMonth(number){
    if (number==0) return('mês');
    if (number==1) return('mês');
    return('meses');
}

function showYear(number){
    if (number==0) return('ano');
    if (number==1) return('ano');
    return('anos');
}