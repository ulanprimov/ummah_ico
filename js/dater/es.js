function showSecond(number){
    if (number==0) return('segundos');
    if (number==1) return('segundo');
    return('segundos');
}

function showMinute(number){
    if (number==0) return('minutos');
    if (number==1) return('minuto');
    return('minutos');
}

function showHour(number){
    if (number==0) return('horas');
    if (number==1) return('hora');
    return('horas');
}

function showDay(number){
    if (number==0) return('días');
    if (number==1) return('día');
    return('días');
}

function showMonth(number){
    if (number==0) return('mes');
    if (number==1) return('mes');
    return('meses');
}

function showYear(number){
    if (number==0) return('año');
    if (number==1) return('año');
    return('años');
}